#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:289358277ef91ab520548325735e1c2d739d0973; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:8d5b7c758b9578b16e699ebb084b16dee16993bf \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:289358277ef91ab520548325735e1c2d739d0973 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
